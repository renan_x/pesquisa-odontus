# Instalação


## Depêndencias:

- PHP 7 ou maior
- Composer
- Mysql
- Git

# Instalação do projeto:

## Baixando o projeto
git clone https://global_dev@bitbucket.org/global_dev/pesquisa-odontus.git

## Intalando projeto, dentro da pasta odontus
composer install

Para configurar o banco cire um arquivo .env com as seguintes informações:
Em DB_DATABASE, DB_USERNAME, DB_PASSWORD altere os dados do banco.
Salve o arquivo

```
APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:1sx/Zl10vHmBxJ9XaUcL/Ddgp20vLMQgUsLc8GqQERk=
APP_DEBUG=true
APP_LOG_LEVEL=debug
APP_URL=http://localhost

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=odontus
DB_USERNAME=root
DB_PASSWORD=senha

BROADCAST_DRIVER=log
CACHE_DRIVER=file
SESSION_DRIVER=file
SESSION_LIFETIME=120
QUEUE_DRIVER=sync

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1
```


## Rode o banco

php artisan migrate

## crie os acessos

php artisan db:seed