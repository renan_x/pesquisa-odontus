@extends('layouts.app')

@section('style')

 <link href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.css" rel="stylesheet">

@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                     <div class="row">
                    <div class="col-md-4">Painel</div>
                    <div class="col-md-8 text-right">
                      {!! Form::open(['route' => ['home'],  'method' => 'get',  'class' => 'form-inline']) !!}
                        <form action="{{ route('home') }}" method="get" >
                          <div class="form-group">
                            <label for="start" class="sr-only">Início</label>
                            <input type="date" class="form-control" id="start" value="{{ Request()->start }}" name="start" required placeholder="Início">
                          </div>
                          <div class="form-group">
                            <label for="end" class="sr-only" >Fim</label>
                            <input type="date" class="form-control" name="end" value="{{ Request()->end }}" required id="end">
                          </div>
                          <button type="submit" class="btn btn-primary mb-2">FILTRAR</button>
                          <a href="{{ route('home') }}" class="btn btn-primary mb-2">LIMPAR FILTRO</a>
                          <a href="{{ route('suggestions') }}" class="btn btn-primary mb-2">VER SUGESTÕES</a>
                        {!! Form::close() !!}
                    </div>
                    </div>
                </div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-md-12">
                           <p>1. Probabilidade da Clínica de ser indicada</p> 
                           <div class="col-md-8 col-md-offset-2">
                                <canvas id="q1" width="800" height="400"></canvas>
                           </div>
                        </div>

                        <div class="col-md-12">
                          <br>
                           <p>2. Descrição dos serviços da clínica</p>
                           <div class="col-md-4 col-md-offset-4">
                                <canvas id="q2" width="400" height="400"></canvas>
                           </div>
                        </div>

                        <div class="col-md-12">
                           <p>3. Valor dos serviços prestados</p>
                           <div class="col-md-4 col-md-offset-4">
                                <canvas id="q3" width="400" height="400"></canvas>
                           </div>
                        </div>


                        <div class="col-md-12">
                           <br>
                           <p>4. Atenção ao tratamento recebido do dentista durante o atendimento</p>
                           <div class="col-md-4 col-md-offset-4">
                                <canvas id="q4" width="400" height="400"></canvas>
                           </div>
                        </div>

                        <div class="col-md-12">
                           <br>
                           <p>5. Atenção ao tratamento recebido dos outros profissionais da equipe (Recepcionista/atendentes). </p>
                           <div class="col-md-4 col-md-offset-4">
                                <canvas id="q5" width="400" height="400"></canvas>
                           </div>
                        </div>


                        <div class="col-md-12">
                           <br>
                           <p>6. Tempo de espera entre a sua chegada e o atendimento. </p>
                           <div class="col-md-4 col-md-offset-4">
                                <canvas id="q6" width="400" height="400"></canvas>
                           </div>
                        </div>

                        <div class="col-md-12">
                           <br>
                           <p>7. Limpeza da clínica.</p>
                           <div class="col-md-4 col-md-offset-4">
                                <canvas id="q7" width="400" height="400"></canvas>
                           </div>
                        </div>

                        <div class="col-md-12">
                           <br>
                           <p>8. Satisfação com o tratamento.</p>
                           <div class="col-md-4 col-md-offset-4">
                                <canvas id="q8" width="400" height="400"></canvas>
                           </div>
                        </div>

                         <div class="col-md-12">
                           <br>
                           <p>9. Aviso de Check up</p>
                           <div class="col-md-4 col-md-offset-4">
                                <canvas id="q9" width="400" height="400"></canvas>
                           </div>
                        </div>

                        <div class="col-md-12">
                           <br>
                           <p>10. Preferência para receber lembretes.</p>
                           <div class="col-md-4 col-md-offset-4">
                                <canvas id="q10" width="400" height="400"></canvas>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>


<script type="text/javascript">

var ctx = document.getElementById('q1');

new Chart(ctx, {
    type: 'bubble', 
    data: {
        datasets: [{
            backgroundColor: ['#00acc1', '#00897b', '#66bb6a', '#fbc02d', '#bdbdbd', '#7e57c2', '#ec407a', '#546e7a', '#c2185b', '#ffe082', '#f57f17'],
            label: 'Gráfico de bolhas',
            data: [
              @foreach($q1 as $k => $q)
                {
                    x: {{ $k }},
                    y: {{ $q }},
                    r: 10
                },
              @endforeach
            ],

        }],

        label: [
            'Red'
        ]
    },

});



//q2
var ctx = document.getElementById('q2');
var colors = ['#00acc1', '#00897b', '#66bb6a', '#fbc02d', '#bdbdbd', '#7e57c2', '#ec407a'];

new Chart(ctx, {
    type: 'doughnut', 
    data: {
        datasets: [{
            data: {{ $q2->pluck('size') }},
            backgroundColor: colors
        }],

        labels: {!! $q2->pluck('r') !!} 
    },

});

//q3
var ctx = document.getElementById('q3');
new Chart(ctx, {
    type: 'bar',
    data: {
        datasets: [{
            backgroundColor: ['#c0ca33', '#fbc02d', '#546e7a'],
            label:  'Gráfico de barras',
            data: {{ $q3->pluck('size') }}
        }],

        labels: {!! $q3->pluck('r') !!} 
    }
});

//q4
var ctx = document.getElementById('q4');
new Chart(ctx, {
    type: 'bar',
    data: {
        datasets: [{
            label:  'Gráfico de barras',
            backgroundColor: colors,
            data: {{ $q4->pluck('size') }}
        }],

        labels: {!! $q4->pluck('r') !!} 
    }
});

//q5
var ctx = document.getElementById('q5');
new Chart(ctx, {
    type: 'bar',
    data: {
        datasets: [{
          backgroundColor: ['#bdbdbd', '#7e57c2', '#ec407a'],
          label:  'Gráfico de barras',
          data: {{ $q5->pluck('size') }}
        }],

        labels: {!! $q5->pluck('r') !!} 
    }
});


//q6
var ctx = document.getElementById('q6');
new Chart(ctx, {
    type: 'pie', 
    data: {
        datasets: [{
            backgroundColor: ['#c0ca33', '#ec407a'],
            data: {{ $q6->pluck('size') }}
        }],

        labels: {!! $q6->pluck('r') !!} 
    }
});


//q7
var ctx = document.getElementById('q7');
new Chart(ctx, {
    type: 'bar',
    data: {
        datasets: [{
          backgroundColor: colors,
          label: 'Gráfico de barras',
          data: {{ $q7->pluck('size') }}
        }],
        labels: {!! $q7->pluck('r') !!} 
    }
});

//q8
var ctx = document.getElementById('q8');
new Chart(ctx, {
    type: 'bar',
    data: {
        datasets: [{
          backgroundColor: ['#bdbdbd', '#7e57c2', '#ec407a'],
          label: 'Gráfico de barras',
          data: {{ $q8->pluck('size') }}
        }],
        labels: {!! $q8->pluck('r') !!} 
    }
});


//q8
var ctx = document.getElementById('q9');
new Chart(ctx, {
    type: 'pie', 
    data: {
        datasets: [{
          backgroundColor: ['#66bb6a', '#fbc02d'],
          data: {{ $q9->pluck('size') }}
        }],

        labels: [
            'SIM',
            'NÃO',
        ]
    }
});

var ctx = document.getElementById('q10');

new Chart(ctx, {
    type: 'doughnut', 
    data: {
        datasets: [{
          backgroundColor: colors,
          data: {{ $q10->pluck('size') }}
        }],

        labels: {!! $q10->pluck('r') !!} 
    }
});


</script>
@endsection
