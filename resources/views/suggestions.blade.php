@extends('layouts.app')

@section('style')

 <link href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.css" rel="stylesheet">

@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                     <div class="row">
                    <div class="col-md-4">Painel</div>
                    <div class="col-md-8 text-right">
        
                          <a href="{{ route('home') }}" class="btn btn-primary mb-2">VOLTAR PARA GRÁFICOS</a>
                    </div>
                    </div>
                </div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-md-12">
                          <br>
                          <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th scope="col">Sugestão</th>
                                  <th scope="col">Data</th>
                                </tr>
                              </thead>
                              <tbody>
                                @foreach($q as $qq)
                                <tr>
                                  <td>{{ $qq->r }}</td>
                                  <td width="160">{{ date('d/m/Y H:i:s' , strtotime($qq->created_at)) }}</td>
                                </tr>
                                @endforeach
                              </tbody>

         
                            </table>
                            <div class="text-center ">
                                <div class="left">
                                  Exibindo Página {!! $q->currentPage() !!} de {!! $q->lastPage() !!}.  Total de Registros: {{ $q->total() }}
                                </div>

                                <div class="center-align">
                                  {!! $q->appends(Request::except('page'))->links() !!}
                                </div>
                              </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')

@endsection
