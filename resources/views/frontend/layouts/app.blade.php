<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Pesquisa de satisfação - Odontus</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Lato Font -->
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700|Montserrat:200,500' rel='stylesheet' type='text/css'>

    <script src="https://js.pusher.com/4.4/pusher.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.1/css/materialize.min.css">

    <style type="text/css">

    .radios {
      text-decoration: none;
      text-transform: uppercase;
      border-radius: 25px;
      -webkit-transition: 0.3s;
      -moz-transition: 0.3s;
      -ms-transition: 0.3s;
      -o-transition: 0.3s;
      transition: 0.3s;
      box-shadow: 0 2px 3px rgba(0,0,0,0.5);
      border: none; 
      font-size: 12px;
      text-align: center;
    }

    .montserrat {
      font-family:  'Montserrat', sans-serif;
    }

    </style>
  @yield('style')
</head>
<body>
    <div class="container">
        <div class="section">
        @yield('content')
        </div>
    </div>

    @yield('pos-content')

  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
     
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    
    <script type="text/javascript">
        $(".dropdown-trigger").dropdown();
        $('.tooltipped').tooltip();
        $('.tabs').tabs();  

    </script>

  
    @yield('script')

</body>
</html>
