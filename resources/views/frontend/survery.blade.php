@extends('frontend.layouts.app')


@section('style')
  <link rel="stylesheet" href="https://unpkg.com/materialize-stepper@3.1.0/dist/css/mstepper.min.css">
@endsection

@section('content')

<div class="col s12 center-align montserrat"><h5>PESQUISA DE SATISFAÇÃO<br>EQUIPE ODONTUS
</h5></div>
<div class="col s12 center-align montserrat"><p>Prezado(a) paciente,<br> 
Respondendo a este questionário você irá nos ajudar a melhorar cada vez mais nossos serviços.<br>
Obrigado pela colaboração!
</p></div>

{!! Form::open(['route' => ['satisfaction.survery.store'],  'method' => 'post']) !!}
<ul class="stepper horizontal">

  <li class="step active">
      <div class="step-title waves-effect"></div>
      <div class="step-content">
         <!-- Your step content goes here (like inputs or so) -->
         <div class="col s12 montserrat">
              <p>1. Qual a probabilidade de você indicar nossa Clínica para algum amigo ou conhecido? (numa escala de 0 a 10).
              </p>
              <p class="range-field">
                <input type="range" name="q1" min="0" max="10" />
              </p>
          </div>
         <div class="step-actions">
            <!-- Here goes your actions buttons -->
            <button class="waves-effect waves-dark btn next-step">PRÓXIMA</button>
         </div>
      </div>
   </li>

  <li class="step">
      <div class="step-title waves-effect"></div>
      <div class="step-content">
         <!-- Your step content goes here (like inputs or so) -->
         <div class="col s12 montserrat">
            <p>2. Qual das seguintes palavras você usaria para descrever os nossos serviços? Selecione tudo que achar adequado.
            </p>

            <p>
              <label>
                <input type="checkbox"  name="q2[0]"  class="filled-in" />
                <span>Confiável</span>
              </label>
            </p>
            <p>
              <label>
                <input type="checkbox" name="q2[1]"  class="filled-in" />
                <span>Não confiável</span>
              </label>
            </p>
            <p>
              <label>
                <input type="checkbox" name="q2[2]"  class="filled-in" />
                <span>Alta Qualidade</span>
              </label>
            </p>
            <p>
              <label>
                <input type="checkbox" name="q2[3]" class="filled-in" />
                <span>Baixa Qualidade</span>
              </label>
            </p>
            <p>
              <label>
                <input type="checkbox" name="q2[4]" class="filled-in" />
                <span>Útil</span>
              </label>
            </p>
            
            <p>
              <label>
                <input type="checkbox" name="q2[5]" class="filled-in" />
                <span>Único</span>
              </label>
            </p>

            <!--
            <p>
              <label>
                <input type="checkbox" name="q2[6]" class="filled-in" />
                <span>Ineficaz</span>
              </label>
            </p>

          -->

            <br>
        </div>
         <div class="step-actions">

            <button class="waves-effect waves-dark btn next-step right-align">PRÓXIMA</button>

            <button class="waves-effect waves-dark btn-flat previous-step left-align">ANTERIOR</button>
         </div>
      </div>
   </li>


   <li class="step">
      <div class="step-title waves-effect"></div>
      <div class="step-content">
         <div class="col s12 montserrat">
            <p>3. Como classifica o valor dos serviços recebidos?</p>
            <p>
              <label>
                <input class="with-gap" name="q3" value="Baixo custo" type="radio"  />
                <span>Baixo custo</span>
              </label>
            </p>
            <p>
              <label>
                <input class="with-gap" name="q3" value="Alto Custo Benefício" type="radio"  />
                <span>Alto Custo Benefício </span>
              </label>
            </p>
            <p>
              <label>
                <input class="with-gap" name="q3" value="Valor Justo" type="radio"  />
                <span>Valor Justo</span>
              </label>
            </p>
        </div>
         <div class="step-actions">

            <button class="waves-effect waves-dark btn next-step right-align">PRÓXIMA</button>
            
            <button class="waves-effect waves-dark btn-flat previous-step left-align">ANTERIOR</button>
         </div>
      </div>
   </li>

   <li class="step">
      <div class="step-title waves-effect"></div>
      <div class="step-content">


        <div class="col s12 montserrat">
            <p>4. Como classifica a atenção ao tratamento recebido do dentista durante o atendimento clínico?</p>
                <p>
              <label>
                <input class="with-gap" name="q4" value="Excelente/Bom" type="radio"  />
                <span>Excelente/Bom</span>
              </label>
            </p>
            <p>
              <label>
                <input class="with-gap" name="q4" value="Regular" type="radio"  />
                <span>Regular</span>
              </label>
            </p>
            <p>
              <label>
                <input class="with-gap" name="q4" value="Ruim/Péssima" type="radio"  />
                <span>Ruim/Péssima</span>
              </label>
            </p>
        </div>

         <div class="step-actions">

            <button class="waves-effect waves-dark btn next-step right-align">PRÓXIMA</button>
            
            <button class="waves-effect waves-dark btn-flat previous-step left-align">ANTERIOR</button>
         </div>
      </div>
   </li>

   <li class="step">
      <div class="step-title waves-effect"></div>
      <div class="step-content">

        <div class="col s12 montserrat">
            <p>5. Como classifica a atenção ao tratamento recebido dos outros profissionais da equipe? (Recepcionista/atendentes).
            </p>

            <p>
              <label>
                <input class="with-gap" name="q5" value="Excelente/Bom" type="radio"  />
                <span>Excelente/Bom</span>
              </label>
            </p>
            <p>
              <label>
                <input class="with-gap" name="q5"  value="Regular" type="radio"  />
                <span>Regular</span>
              </label>
            </p>
            <p>
              <label>
                <input class="with-gap" name="q5" value="Ruim/Péssima"  type="radio"  />
                <span>Ruim/Péssima</span>
              </label>
            </p>
        </div>

         <div class="step-actions">

            <button class="waves-effect waves-dark btn next-step right-align">PRÓXIMA</button>
            
            <button class="waves-effect waves-dark btn-flat previous-step left-align">ANTERIOR</button>
         </div>
      </div>
   </li>

   <li class="step">
      <div class="step-title waves-effect"></div>
      <div class="step-content">
          <div class="col s12 montserrat">
            <p>6. Como classifica o tempo de espera entre a sua chegada e o atendimento?
            </p>

            <p>
              <label>
                <input class="with-gap" name="q6" value="Muito curto/curto" type="radio"  />
                <span>Muito curto/curto</span>
              </label>
            </p>
            <p>
              <label>
                <input class="with-gap" name="q6" value="Nem muito longo, nem curto" type="radio"  />
                <span>Nem muito longo, nem curto </span>
              </label>
            </p>
            <p>
              <label>
                <input class="with-gap" name="q6" value="Longo/Muito longo" type="radio"  />
                <span>Longo/Muito longo</span>
              </label>
            </p>
        </div>
         <div class="step-actions">

            <button class="waves-effect waves-dark btn next-step right-align">PRÓXIMA</button>
            
            <button class="waves-effect waves-dark btn-flat previous-step left-align">ANTERIOR</button>
         </div>
      </div>
   </li>

   <li class="step">
      <div class="step-title waves-effect"></div>
      <div class="step-content">

        <div class="col s12 montserrat">
            <p>7. Como considera a limpeza da clínica?

            </p>

            <p>
              <label>
                <input class="with-gap" name="q7" value="Excelente/Bom" type="radio"  />
                <span>Excelente/Bom</span>
              </label>
            </p>
            <p>
              <label>
                <input class="with-gap" name="q7" value="Regular" type="radio"  />
                <span>Regular</span>
              </label>
            </p>
            <p>
              <label>
                <input class="with-gap" name="q7" value="Ruim/Péssima" type="radio"  />
                <span>Ruim/Péssima</span>
              </label>
            </p>
        </div>

         <div class="step-actions">

            <button class="waves-effect waves-dark btn next-step right-align">PRÓXIMA</button>
            
            <button class="waves-effect waves-dark btn-flat previous-step left-align">ANTERIOR</button>
         </div>
      </div>
   </li>

   <li class="step">
      <div class="step-title waves-effect"></div>
      <div class="step-content">
        <div class="col s12 montserrat">
            <p>8. Quanto você está satisfeito com o tratamento?
            </p>


            <p>
              <label>
                <input class="with-gap" name="q8" value="Totalmente satisfeito"  type="radio"  />
                <span>Totalmente satisfeito</span>
              </label>
            </p>
            <p>
              <label>
                <input class="with-gap" name="q8"  value="Satisfeito" type="radio"  />
                <span>Satisfeito</span>
              </label>
            </p>
            <p>
              <label>
                <input class="with-gap" name="q8" value="Pouco Satisfeito" type="radio"  />
                <span>Pouco Satisfeito</span>
              </label>
            </p>
        </div>
         <div class="step-actions">

            <button class="waves-effect waves-dark btn next-step right-align">PRÓXIMA</button>
            
            <button class="waves-effect waves-dark btn-flat previous-step left-align">ANTERIOR</button>
         </div>
      </div>
   </li>
   <li class="step">
      <div class="step-title waves-effect"></div>
      <div class="step-content">


        <div class="col s12 montserrat">
            <p>9. Você gostaria de ser avisado quando for tempo de retornar para um Check up?
            </p>

            <p>
              <label>
                <input class="with-gap" name="q9" value="Sim" type="radio"  />
                <span>Sim</span>
              </label>
            </p>
            <p>
              <label>
                <input class="with-gap" name="q9" value="Não" type="radio"  />
                <span>Não</span>
              </label>
            </p>

        </div>
         <div class="step-actions">

            <button class="waves-effect waves-dark btn next-step right-align">PRÓXIMA</button>
            
            <button class="waves-effect waves-dark btn-flat previous-step left-align">ANTERIOR</button>
         </div>
      </div>
   </li>

   <li class="step">
      <div class="step-title waves-effect"></div>
      <div class="step-content">


        <div class="col s12 montserrat">
            <p>10. De que forma prefere receber esse lembrete?
            </p>

            <p>
              <label>
                <input type="checkbox" name="q10[0]" class="filled-in" />
                <span>Whatsapp</span>
              </label>
            </p>
            <p>
              <label>
                <input type="checkbox" name="q10[1]" class="filled-in" />
                <span>SMS</span>
              </label>
            </p>
            <p>
              <label>
                <input type="checkbox" name="q10[2]" class="filled-in" />
                <span>E-mail</span>
              </label>
            </p>
            <p>
              <label>
                <input type="checkbox" name="q10[3]" class="filled-in" />
                <span>Não quero receber lembrete</span>
              </label>
            </p>
        </div>

         <div class="step-actions">

            <button class="waves-effect waves-dark btn next-step right-align">PRÓXIMA</button>
            
            <button class="waves-effect waves-dark btn-flat previous-step left-align">ANTERIOR</button>
         </div>
      </div>
   </li>
   
   <li class="step">
      <div class="step-title waves-effect"></div>
      <div class="step-content">


        <div class="col s12 montserrat">
            <p>11. O que você sugere para tornar sua visita ao dentista mais prazerosa?
            </p>


            <div class="input-field col s12">
                  <textarea id="textarea1" class="materialize-textarea" name="q11"></textarea>
                  <label for="textarea1">Digite aqui!</label>
            </div>
        </div>

         <div class="step-actions">

            <button class="waves-effect waves-dark btn right-align" type="submit">FINALIZAR</button>
            
            <button class="waves-effect waves-dark btn-flat previous-step left-align">ANTERIOR</button>
         </div>
      </div>
   </li>   

</ul>
{!! Form::close() !!}

@endsection

@section('script')
<script src="https://unpkg.com/materialize-stepper@3.1.0/dist/js/mstepper.min.js">

</script>

<script>
   var stepper = document.querySelector('.stepper');
   var stepperInstace = new MStepper(stepper, {
      // options
      firstActive: 0, // this is the default
      stepTitleNavigation: false,

      validationFunction: defaultValidationFunction,

   });


  function defaultValidationFunction(stepperForm, activeStepContent) {
     var inputs = activeStepContent.querySelectorAll('input, textarea, select, checkbox');



     //console.log(inputs[1].checked);
     //console.log(inputs[1].type);

     for (let i = 0; i < inputs.length; i++){

       switch(inputs[i].type){
          case 'checkbox':
              if (inputs[i].checked) {
                return true;
              }

            break;

          case 'radio':
              if (inputs[i].checked) {
                return true;
              }
            break;

          default:
            if (inputs[i].checkValidity()) 
              return true;

       }

      }

     //if ('checkbox') {};
     //var vality = false;
     /*
     for (let i = 0; i < inputs.length; i++){
        if (inputs[i].checkValidity()) 
          return true;
     } */
     return false;
  }





</script>
   
@endsection
