<?php

use Illuminate\Database\Seeder;

use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	if(User::where('email','=', 'equipeodontus@gmail.com')->count() == 0) {
    		User::firstOrCreate([
	            'name' => 'Equipe Odontus',
	            'email' => 'equipeodontus@gmail.com',
	            'password' =>  bcrypt('equipe2016'),
	         ]);
    	}
    }
}
