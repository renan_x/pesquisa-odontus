<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Question;

class SatisfactionSurveryController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.survery');
    }


    public function store(Request $request)
    {
        /*
        * Tipos
        * 1 = Escolha simples
        * 2 = Multipla escolha
        * 3 = texto
        */

    	$inputs = $request->all();
    	unset($inputs['_token']);
    	$names;
        $c = 0;

        $mulple1 = ['Confiável', 'Não confiável', 'Alta Qualidade', 'Baixa Qualidade', 'Útil', 'Único', 'Ineficaz'];
        $mulple2 = ['Whatsapp', 'SMS', 'E-mail', 'Não quero receber lembrete'];
    	
        foreach ($inputs as $key => $value) {

    		$names[] = $key;

            switch ($c) {

                //Multipla escolha
                case 1:

                    if (is_array($value)) {

                        foreach ($value as $keyV => $valueV) {
                            Question::create(['question' => $c, 'type' =>  2, 'r' => $mulple1[$keyV] ]);
                        }
                        
                    } else {
                        Question::create(['question' => $c, 'type' =>  2, 'r' => $value]);
                    }

                    //Question::create(['question' => $c, 'type' =>  1, 'r' => $value]);
                    break;                

                //Multipla escolha
                case 9:

                    if (is_array($value)) {

                        foreach ($value as $keyV => $valueV) {
                            Question::create(['question' => $c, 'type' =>  2, 'r' => $mulple2[$keyV] ]);
                        }
                        
                    } else {
                        Question::create(['question' => $c, 'type' =>  2, 'r' => $value]);
                    }
                    break;

                //Texto
                case 10:
                    if (strlen($value) > 3) {
                        Question::create(['question' => $c, 'type' =>  3, 'r' => $value]);
                    }
                    
                    break;
                
                default:
                    Question::create(['question' => $c, 'type' =>  1, 'r' =>  $value]);
                    break;
            }

    		
            $c ++;
    	}
    	
    	return redirect()->route('satisfaction.thanks');
    
    }



}
