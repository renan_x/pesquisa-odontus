<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Question;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $filter = "created_at IS NOT NULL";

        if (!$request->all() == null) {
            if ($request->start > $request->end) {
                return redirect()->route('home');
            } else {
                
                    $filter = "questions.created_at  >= \"" . $request->start ."\" AND created_at  <= \"" . $request->end . " 23:59:00\"";
            
            }
        } 




        $q1;
        for ($i=0; $i < 11 ; $i++) { 
            $q1[] = Question::where('type', 1)
                ->whereRaw($filter)
                ->where('question', 0)
                ->where('r', $i)
                ->count();  
        }

        $q2 = Question::where('type', 2)
                ->where('question', 1)
                ->whereRaw($filter)
                ->selectRaw("COUNT(id) as size, r")
                ->groupBy("r")
                ->get();        

        $q3 = Question::where('type', 1)
                ->where('question', 2)
                ->whereRaw($filter)
                ->selectRaw("COUNT(id) as size, r")
                ->groupBy("r")
                ->get();

        $q4 = Question::where('type', 1)
                ->where('question', 3)
                ->whereRaw($filter)
                ->selectRaw("COUNT(id) as size, r")
                ->groupBy("r")
                ->get();        

        $q5 = Question::where('type', 1)
                ->where('question', 4)
                ->whereRaw($filter)
                ->selectRaw("COUNT(id) as size, r")
                ->groupBy("r")
                ->get();

        $q6 = Question::where('type', 1)
                ->where('question', 5)
                ->whereRaw($filter)
                ->selectRaw("COUNT(id) as size, r")
                ->groupBy("r")
                ->get();

        $q7 = Question::where('type', 1)
                ->where('question', 6)
                ->whereRaw($filter)
                ->selectRaw("COUNT(id) as size, r")
                ->groupBy("r")
                ->get();

        $q8 = Question::where('type', 1)
                ->where('question', 7)
                ->whereRaw($filter)
                ->selectRaw("COUNT(id) as size, r")
                ->groupBy("r")
                ->get();

        $q9 = Question::where('type', 1)
                ->where('question', 8)
                ->whereRaw($filter)
                ->selectRaw("COUNT(id) as size, r")
                ->groupBy("r")
                ->get();

        $q10 = Question::where('type', 2)
                ->where('question', 9)
                ->whereRaw($filter)
                ->selectRaw("COUNT(id) as size, r")
                ->groupBy("r")
                ->get();


        return view('home', compact('q1', 'q2', 'q3', 'q4', 'q5', 'q6', 'q7', 'q8', 'q9', 'q10'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function suggestions()
    {
         $q = Question::where('type', 3)
                ->where('question', 10)
                ->orderby('id', 'desc')
                ->paginate(30);

        return view('suggestions', compact('q'));
    }
}
