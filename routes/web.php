<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/suggestions', 'HomeController@suggestions')->name('suggestions');

Route::get('/pesquisa-de-satisfacao', 'SatisfactionSurveryController@index')->name('satisfaction.survery');
Route::post('/pesquisa-de-satisfacao', 'SatisfactionSurveryController@store')->name('satisfaction.survery.store');


Route::get('/obrigado',  function () {
    return view('frontend.thanks');
})->name('satisfaction.thanks');
